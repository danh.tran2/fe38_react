import React, { Component } from "react";

class EventHandling extends Component {
  showMessage = () => {
    alert("Hello 2");
  };
  showProfile = (profile) => {
    console.log(profile);
  };

  //   explain
  showProfileCallBack = (hocVien, _this) => {
    console.log(hocVien);
    console.log(_this);
  };
  
  render() {
    let hocVien = {
      ma: 1,
      ten: "Lê Văn Tèo",
      lop: "frontend 38",
    };

    return (
      <div>
        {/* Cách 1 : xử lý sự kiện dùng function 1 lần */}
        <button
          onClick={() => {
            alert("hello");
          }}
        >
          CLick me
        </button>
        {/* Cách 2L xử lý sự kiện dùng function nhiều lần */}
        <button onClick={this.showMessage}> Click 2</button>
        {/* Xử lý sự kiện với tham số */}
        <button
          onClick={() => {
            this.showProfile(hocVien);
          }}
        >
          show profile
        </button>
        <button onClick={this.showProfile.bind(this, hocVien)}> Show profile callback</button>
      </div>
    );
  }
}

export default EventHandling;
