import React, { Component } from "react";

class DataBinding extends Component {
  userName = "Danh fe38";
  hocVien = {
    maHV: 1,
    tenHV: "Nguyễn Văn a",
    tuoi: 19,
  };

  renderImg = () => {
    //   khi binding data = phương thức thì dữ liệu trả về phải là component hoặc text, number
    return (
      <img src="https://ec.europa.eu/programmes/creative-europe/sites/creative-europe/files/covid19-cdc-unsplash.jpg" />
    );
  };

  render() {
    let { maHV, tenHV, tuoi } = this.hocVien;
    // bien var let const
    const age = 20;
    return (
      <div>
        {this.renderImg()}
        <p>{this.userName}</p>
        <p id="text">{age}</p>
        <ul>
          <li>mã: {maHV}</li>
          <li>mã: {tenHV}</li>
          <li>mã: {tuoi}</li>
        </ul>
      </div>
    );
  }
}

export default DataBinding;
