import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
// import DataBinding from "./DataBinding";
// import EventHandling from "./EventHandling";
// import StateUpdating from "./StateUpdating/StateUpdating";
// import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import PropDemo from "./PropsDemo/PropDemo";
// import BaiTapThuKinh from "./BaiTapThuKinh/BaiTapThuKinh";
import DanhSachSanPham from "./PropsDemo/DanhSachSanPham";
import Modal from "./PropsDemo/Modal";

class App extends Component {
  state = {
    thongTinSanPham: {
      tenSP: "Tên mặc định",
      hinhAnh: "",
      gia: 1000,
      moTa: "trống",
    },
  };

  xemChiTiet = (sanPhamChiTiet) => {
    // lấy thông tin chi tiết sản phẩm
    this.setState({ thongTinSanPham: sanPhamChiTiet });
  };

  render() {
    let sinhVien = {
      ma: 1,
      ten: "Tèo",
      tuoi: 19,
    };

    let mangSP = [
      {
        maSP: 1,
        tenSP: "black car",
        hinhAnh: "./img/products/black-car.jpg",
        gia: 1000,
      },
      {
        maSP: 2,
        tenSP: "red car",
        hinhAnh: "./img/products/red-car.jpg",
        gia: 2000,
      },
      {
        maSP: 3,
        tenSP: "silver car",
        hinhAnh: "./img/products/silver-car.jpg",
        gia: 2000,
      },
      {
        maSP: 4,
        tenSP: "steel car",
        hinhAnh: "./img/products/steel-car.jpg",
        gia: 2000,
      },
    ];

    return (
      <div>
        {/* <DataBinding /> */}
        {/* <EventHandling /> */}
        {/* <StateUpdating /> */}
        {/* <RenderWithMap /> */}
        {/* <PropDemo title="FrontEnd 38" ttSinhVien={sinhVien} /> */}
        {/* <BaiTapThuKinh /> */}
        {/* <BaiTapChonXe /> */}
        <DanhSachSanPham mangSanPham={mangSP} xemChiTiet={this.xemChiTiet} />
        <Modal thongTinSanPham={this.state.thongTinSanPham} />
      </div>
    );
  }
}

export default App;
