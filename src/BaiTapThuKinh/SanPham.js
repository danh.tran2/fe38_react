import React, { Component } from "react";

export default class SanPham extends Component {
  render() {
    let { sp } = this.props;
    return (
      <img
        onClick={() => {
          this.props.xemChiTiet(sp);
        }}
        className="img-fluid my-4"
        src={sp}
      />
    );
  }
}
