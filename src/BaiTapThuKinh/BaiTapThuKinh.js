import React, { Component } from "react";
import styleGlasses from "./BaiTapThuKinh.module.css";
import SanPham from "./SanPham";

export default class BaiTapThuKinh extends Component {
  arrProduct = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./glassesImage/v1.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: "./glassesImage/v2.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: "./glassesImage/v3.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 4,
      price: 30,
      name: "DIOR D6005U",
      url: "./glassesImage/v4.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 5,
      price: 30,
      name: "PRADA P8750",
      url: "./glassesImage/v5.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 6,
      price: 30,
      name: "PRADA P9700",
      url: "./glassesImage/v6.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 7,
      price: 30,
      name: "FENDI F8750",
      url: "./glassesImage/v7.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 8,
      price: 30,
      name: "FENDI F8500",
      url: "./glassesImage/v8.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 9,
      price: 30,
      name: "FENDI F4300",
      url: "./glassesImage/v9.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  ];

  state = {
    // thongTinSanPham: {
    //   id: 2,
    //   price: 50,
    //   name: "GUCCI G8759H",
    //   url: "./glassesImage/v2.png",
    //   desc:
    //     "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    // },
    url: "./glassesImage/v2.png",
  };

  xemChiTiet = (sanPhamChiTiet) => {
    // lấy thông tin chi tiết sản phẩm
    // this.setState({ thongTinSanPham: sanPhamChiTiet });
    this.setState({ url: sanPhamChiTiet });
  };

  renderProductList = () => {
    return this.arrProduct.map((sanPham, index) => {
      return (
        <div key={index} className="col-4">
          {/* <SanPham sp={sanPham} xemChiTiet={this.xemChiTiet} /> */}
          <SanPham sp={sanPham.url} xemChiTiet={this.xemChiTiet} />
        </div>
      );
    });
  };

  render() {
    // let numRandom = Math.round(Math.random());
    let numRandom = Date.now();

    // Tạo số ngẫu nhiên

    let animationKeyFrame = `@keyframes animChangeGlass${numRandom} {
        from{
            width: 0;
            top: 0;
            opacity: 0;
        }
        to {
            width: 250px;
            top: 155px;
            opacity: 0.8;
        }
    }`;

    const styleGlassesDefault = {
      position: "absolute",
      width: "270px",
      left: "105px",
      top: "150px",
      opacity: "0.8",
      animation: `animChangeGlass${numRandom} 1s`,
    };

    return (
      <div
        style={{
          backgroundImage: "url(./glassesImage/bgImg.jpg)",
          minHeight: 2000,
        }}
      >
        <style>
            {animationKeyFrame}
        </style>
        <div style={{ backgroundColor: "rgba(0,0,0,0.5)", minHeight: 2000 }}>
          <h3 style={{ backgroundColor: "rbga(255,127,36,0.5)" }}>
            BÀI TẬP THỬ KÍNH
          </h3>
          <div className="container-fluid">
            <div className="row">
              <div className="col-7">
                <div className="card">
                  <div className="card-header bg-light">Danh sách mẫu kính</div>
                  <div className="card-body">
                    <div className="row">{this.renderProductList()}</div>
                  </div>
                </div>
              </div>
              <div className="col-5">
                <div className="position-relative">
                  <img
                    className="position-absolute"
                    src="./glassesImage/model.jpg"
                    alt="model"
                  />
                  <img
                  
                  // src={this.state.thongTinSanPham.url}
                  src={this.state.url}
                  style={styleGlassesDefault}
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
