import React, { Component } from "react";

class StateUpdating extends Component {
  // state: thuộc tính chứa các giá trị có thể thay đổi khi người dùng thao tác

  state = {
    isLogin: false,
  };
  //   isLogin = false;
  userName = "Hello bois";

  login = () => {
    // this.state.isLogin = true;
    // Không được gán giá trị trực tiếp cho state
    // => Phải thông qua phương thức setState
    // let newState = {
    //     isLogin: true
    // }
    // this.setState là 1 phương thức của class Component mà Component mình tạo kế thừa: Dùng để set giá trị các biến trong State và gọi lại hàm render

    // this.setState(newState);
    // Viết gọn lại
    this.setState({
      isLogin: true,
    });
    console.log(this.state.isLogin);
  };

  renderLogin = () => {
    if (this.isLogin) {
      return <p>{this.userName}</p>;
    }
    return (
      <button
        onClick={() => {
          this.login();
        }}
      >
        Đăng nhập
      </button>
    );
  };

  render() {
    // return <div></div>;
    return (
      <div>
        {this.renderLogin()}
        {this.isLogin ? (
          <p>{this.userName}</p>
        ) : (
          <button> Đăng nhập 3 ngôi</button>
        )}
      </div>
    );
  }
}

export default StateUpdating;
