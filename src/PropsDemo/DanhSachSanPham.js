import React, { Component } from "react";
import SanPham from "./SanPham";
 
class DanhSachSanPham extends Component {

  renderSanPham = () => {
    let { mangSanPham } = this.props;
    return mangSanPham.map((sanPham, index) => {
      return (
        <div key={index} className="col-3">
          <SanPham sp={sanPham} xemChiTiet={this.props.xemChiTiet} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <h3>Danh sách sản phẩm</h3>
        <div className="row">{this.renderSanPham()}</div>
      
      </div>
    );
  }
}

export default DanhSachSanPham;
