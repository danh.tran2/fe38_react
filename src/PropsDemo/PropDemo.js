import React, { Component } from "react";

class PropDemo extends Component {
  // Các thuộc tính có sẵn của component đã học tới giờ
  // this.state: dùng lưu gtri thdoi khi user thao tác
  // this.props: dùng nhận dữ liệu từ component cha truyền vào
  // this.setState: dùng để thdoi gtri state và render lại giao diện
  // Phỏng vấn: phân biệt state và props
  // State có thể gán lại giá trị thông qua setState
  // Props không thể gán lại giá trị

  // renderSanPham = () => {
  //     let mangSanPham = this.props.mangSanPham;
  // }

  render() {
    // nhận giá trị sinhVien từ component cha truyền vào
    // sv?.ten là  JavaScript's optional chaining feature, check có sv thì mới trả
    let sv = this.props.ttSinhVien;
    return (
      <div>
        <header className="text-success text-center">{this.props.title}</header>
        <div className="card text-left">
          <div className="card-body">
            <h4 className="card-title">Họ tên: {sv?.ten}</h4>
            <p className="card-text">Tuổi: {sv?.tuoi}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default PropDemo;
