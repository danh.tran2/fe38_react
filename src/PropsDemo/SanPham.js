import React, { Component } from "react";

class SanPham extends Component {
  render() {
    let { sp } = this.props;
    return (
      <div className="card text-left">
        <img className="card-img-top" src={sp.hinhAnh} alt />
        <div className="card-body">
          <h4 className="card-title">{sp.tenSP}</h4>
          <p className="card-text">{sp.gia}</p>
          <button
            onClick={() => {
              this.props.xemChiTiet(sp);
            }}
            className="btn btn-success"
            data-toggle="modal"
            data-target="#modelId"
          >
            Xem chi tiết{" "}
          </button>
        </div>
      </div>
    );
  }
}

export default SanPham;
