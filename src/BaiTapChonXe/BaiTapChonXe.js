import React, { Component } from "react";
// import "./BaiTapChonXe.css";
import styles from "./BaiTapChonXe.module.css";

class BaiTapChonXe extends Component {
  state = {
    imgCar: "./img/products/black-car.jpg",
  };

  chonXe = (imgCarColor) => {
    // thay d9oi63 thuo65c ti2nh src cua3 thẻ img chứa hình ảnh xe
    this.setState({
      imgCar: imgCarColor,
    });
  };

  render() {
    const styleCursor = {
      cursor: "pointer",
    };

    return (
      <div className="container-fluid">
        <h1 className="txtColor">Bài tập chọn xe Cybersoft</h1>
        <h1 className={styles.txtTitle}>Bài tập chọn xe Cybersoft</h1>
        <div className="row">
          <img
            className="col-7"
            // style={{ width: "50%" }}
            src={this.state.imgCar}
            alt=""
          />
          <div className="col-5">
            <div className="card text-left">
              <img className="card-img-top" src="holder.js/100px180/" alt="" />
              <div className="card text-left">
                <div className="card-header bg-light text-success">
                  Chọn màu xe
                </div>
                <div className="card-body">
                  <div
                    style={styleCursor}
                    className="row pt-3 pb-3 mt-3 border border-light"
                    onClick={() => {
                      this.chonXe("./img/products/black-car.jpg");
                    }}
                  >
                    <div className="col-2">
                      <img
                        style={{width: '50px'}}
                        src="./img/CarBasic/icons/icon-black.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-10">
                      <h3>Crystal Black</h3>
                      <p>Pearl</p>
                    </div>
                  </div>
                  <div
                    style={styleCursor}
                    className="row pt-3 pb-3 mt-3 border border-light"
                    onClick={() => {
                      this.chonXe("./img/products/Red-car.jpg");
                    }}
                  >
                    <div className="col-2">
                      <img
                        style={{width: '50px'}}
                        src="./img/CarBasic/icons/icon-Red.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-10">
                      <h3>Crystal Red</h3>
                      <p>Pearl</p>
                    </div>
                  </div>
                  <div
                    style={styleCursor}
                    className="row pt-3 pb-3 mt-3 border border-light"
                    onClick={() => {
                      this.chonXe("./img/products/Steel-car.jpg");
                    }}
                  >
                    <div className="col-2">
                      <img
                        style={{width: '50px'}}
                        src="./img/CarBasic/icons/icon-Steel.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-10">
                      <h3>Crystal Steel</h3>
                      <p>Pearl</p>
                    </div>
                  </div>
                  <div
                    style={styleCursor}
                    className="row pt-3 pb-3 mt-3 border border-light"
                    onClick={() => {
                      this.chonXe("./img/products/Silver-car.jpg");
                    }}
                  >
                    <div className="col-2">
                      <img
                        style={{width: '50px'}}
                        src="./img/CarBasic/icons/icon-Silver.jpg"
                        alt=""
                      />
                    </div>
                    <div className="col-10">
                      <h3>Crystal Silver</h3>
                      <p>Pearl</p>
                    </div>
                  </div>
                  {/* <button
                    onClick={() => {
                      this.chonXe("./img/products/red-car.jpg");
                    }}
                  >
                    Xe đỏ
                  </button>
                  <br />
                  <button
                    onClick={() => {
                      this.chonXe();
                    }}
                  >
                    Xe đen
                  </button>
                  <br />
                  <button
                    onClick={() => {
                      this.chonXe("./img/products/silver-car.jpg");
                    }}
                  >
                    Xe bạc
                  </button>
                  <br />
                  <button
                    onClick={() => {
                      this.chonXe("./img/products/grey-car.jpg");
                    }}
                  >
                    Xe xám
                  </button>
                  <br /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BaiTapChonXe;
